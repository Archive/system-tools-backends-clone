# Vietnamese translation for system-tools-backends-clone.
# Copyright (C) 2011 system-tools-backends-clone's COPYRIGHT HOLDER
# This file is distributed under the same license as the system-tools-backends-clone package.
# Lê Trường An <xinemdungkhoc1@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: system-tools-backends-clone master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-system-tools&keywords=I18N+L10N&component=s-t-b\n"
"POT-Creation-Date: 2011-03-28 21:47+0000\n"
"PO-Revision-Date: 2011-04-20 19:48+0700\n"
"Last-Translator: Lê Trường An <xinemdungkhoc1@gmail.com>\n"
"Language-Team: Vietnamese <gnomevi-list@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:1
msgid "Change the user's own account configuration"
msgstr "Thay đổi cấu hình tài khoản riêng của người dùng"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:2
msgid "Manage system configuration"
msgstr "Quản lí cấu hình hệ thống"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:3
msgid "You need to authenticate to modify the system configuration"
msgstr "Bạn cần xác thực để chỉnh sửa cấu hình hệ thống"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:4
msgid "You need to authenticate to modify your user account information"
msgstr "Bạn cần xác thực để chỉnh sửa thông tin tài khoản người dùng"

