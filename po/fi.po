# Finnish messages for setup-tools-backends
# Copyright (C) 2004 Free Software Foundation, Inc.
# This file is distributed under the same license as the setup-tools-backends package.
# Suomennos: http://gnome-fi.sourceforge.net/
#
# Ilkka Tuohela <hile@iki.fi>, 2004-2009.
# Jiri Grönroos <jiri.gronroos@iki.fi>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: setup-tools-backends\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"system-tools&component=s-t-b\n"
"POT-Creation-Date: 2010-01-13 18:05+0000\n"
"PO-Revision-Date: 2012-09-10 22:15+0300\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>\n"
"Language-Team: Finnish <>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-04-01 17:12+0000\n"
"X-Generator: Lokalize 1.5\n"
"Language: fi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:1
#| msgid "Change user configuration"
msgid "Change the user's own account configuration"
msgstr "Muuta käyttäjän oman tilin asetuksia"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:2
msgid "Manage system configuration"
msgstr "Hallitse järjestelmäasetuksia"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:3
#| msgid "System policy prevents modifying the user configuration"
msgid "You need to authenticate to modify the system configuration"
msgstr "Järjestelmäasetusten muuttaminen vaatii tunnistautumisen"

#: ../org.freedesktop.SystemToolsBackends.policy.in.h:4
msgid "You need to authenticate to modify your user account information"
msgstr "Oman käyttäjätilien tietojen muuttaminen vaatii tunnistautumisen"

#~ msgid "System policy prevents modifying the configuration"
#~ msgstr "Järjestelmän määrittelyt estävät asetusten muuttamisen"
